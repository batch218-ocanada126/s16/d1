// console.log("Hello World")

// [SECTION] Assignment Operator
	// Basic Assignment Operator (=)
	// The assignment operator assigns the value of the right hand operand to a variable
	let assignmentNumber = 8;
	console.log("The value of the assignmentNumber variable: " + assignmentNumber);
	console.log("---------");

// [SECTION] Arithmetic Operations

	let x = 200;
	let y = 18;

	console.log("x: "+ x);
	console.log("y: "+ y);
	console.log("---------");
	
	// Addition
	let sum = x + y;
	console.log("Result of addition Operator: " + sum);

	// Subtraction
	let difference = x - y;
	console.log("Result of subtraction Operator: " + difference);

	// Multiplication
	let	multiplication = x * y;
	console.log("Result of multiplication Operator: " + multiplication);

	//Division
	let qoutient = x / y;
	console.log("Result of qoutient Operator: " + qoutient);

	// Reminder
	let modulo = x % y;
	console.log("Result of modulo Operator: " + modulo);

// Continuation of assignment operator

// Addition Assignment Operator

	// current value of assignmentNumber is 8
	// assignmentNumber = assignmentNumber + 2; // long method
	// console.log(assignmentNumber);

	//short method
	    assignmentNumber += 2;
	    console.log("Result of Addition Assignment Operator: " + assignmentNumber); //assignmentNumber value: 10

	// Subtraction Assignment Operator
	    // assignmentNumber = assignmentNumber - 3;
	    // console.log(assignmentNumber); // 7


	    assignmentNumber -= 3;
	    console.log("Result of Subtraction Assignment Operator: " + assignmentNumber);


	// Multiplication Assignment Operator
	    assignmentNumber *= 2;
	    console.log("Result of Multiplication Assignment Operator: " + assignmentNumber); 

	// Division Assignment Operator
	    assignmentNumber /= 2;
	    console.log("Result of Division Assignment Operator: " + assignmentNumber);

// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of pemdas operation: "+ mdas);
	/*
		The operations were done in the following order:
		1. 3 * 4 = 12 | 1 + 2 - 12 / 5
		2. 12 / 5 = 2.4 | 1 + 2 - 2.4
		3. 1 + 2 = 3 | 3 - 2.4
		4. 3 - 2.4 = 0.6
	*/

let pemdas = (1+(2-3)) * (4/5);
console.log("Result of pemdas operation: "+ pemdas);

//heirarchy
// combinations of multiple arithmetic operators will follow the pemdas rule
/*
	1. parenthesis
	2. exponent
	3. multiplication or division
	4. addition or subtraction

	NOTE: will also follow left to right rule
*/

// [SECTION] Increment and Decrement
// Increment - pagdagdag
// Decrement - pagbawas

// Operators that add or subtract values by 1 and reassign the value of the variable where the increment(++)/decrement(--) was applied.

let z = 1;

//pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment); //2
console.log("Result of pre-increment of z: " + z); //2		    
//post-increment
increment = z++;
console.log("Result of post-increment: " + increment); //2
console.log("Result of post-increment of z: " + z);//3
/*increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment of z: " + z);*/

//pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement); //2
console.log("Result of pre-decrement of z: " + z); //2

//post-decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement); //2
console.log("Result of post-decrement of z: " + z); //1

// [SECTION] Type Coercion
// is the automatic conversion of values from one data type of another
	
	// combination of number and string data type will result to string.
	let numA = 10; // number
	let numB = "12"; // String

	let coercion = numA+numB;
	console.log(coercion);
	console.log(typeof coercion);

	// number and number data type will result to number
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);


	//combination of number and boolean data type will result to number.
	let numX = 10;
	let	numY = true;
			// boolean values are
				// true = 1;
				// false = 0;
	coercion = numX + numY;
	console.log(coercion);
	console.log(typeof coercion);

	let num1 = 1;
	let num2 = false;
	coercion = num1 + num2;
	console.log(coercion);
	console.log(typeof coercion);

// [SECTION] Comparison Operators
//  Comparison operators are ised to evaluate and compare the left and right operands 
// After evaluation, it returns a boolean value

// equality operator (==) - also read as equal
// compares the value, but not the data type
console.log("------------")
console.log(1 == 1); //true
console.log(1 == 2);  //false	
console.log(1 == '1'); //true
console.log(0 == false); //true //false value is also equal to zero

let juan = "juan";
console.log('juan' == 'juan'); //true
console.log('juan' == 'Juan'); //false //equality operator is strict with letter casing.
console.log(juan == "juan"); //true

//Inequality operator (!=) - also read as not equal
console.log("-----------")
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan'!= 'Juan');  //true
console.log(juan != "juan");  //false

// [SECTION] Relational Operators
// Some comparison operators to check wether one value is greater than or less than other value
//it will return a value of true or false

console.log("-------------")

let a = 50;
let b = 65;

//GT or Greater Than Operator(>)
let isGreaterThan = a > b; // 50 > 65 = false
console.log(isGreaterThan);

//LT or Less Than Operator(<)
let isLessThan = a < b; // 50 < 65 = true
console.log(isLessThan);

//GTE or Greater Than or Equal (>=)
let isGTorEqual = a >= b; // false
console.log(isGTorEqual);

//LTE or Less Than or Equal (<=)
let isLTorEqual = a <= b; //true
console.log(isLTorEqual);

// Forced Coercion
let num = 56;
let numStr = "30";
console.log(x > numStr); //true 
//forced coercion to change string to number

let str = "twenty";
console.log(num >= str); //false
// Since the string is not numeric, the string will not be converted to a number, or a.k.a NaN (Not a Number)

// Logical AND operator (&&)
console.log("---------")
let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered; //false
console.log("Result of logical AND Operator: " + allRequirementsMet);
// AND operator requires all / both are true

// Logical OR operator (||)
let someRequirementsMet = isLegalAge || isRegistered; //true
console.log("Result of logical OR Operator: " + someRequirementsMet);
// OR operator requires only 1 true;

// Logical Not Operator (!)
// Returns the opposite value
let someRequirementsNotMet = !isLegalAge //false
console.log("Result of logical NOT Operator: "+ someRequirementsNotMet)